# Linguaggi dinamici

This is an example project for the course of Linguaggi Dinamici that you can download with:
```
git clone https://bitbucket.org/lucatave/linguaggi_dinamici
```

## Dependencies

In order to install all the required dependencies ```python3``` and ```pip``` are required.

## Installation

If you wish to create a virtual environment for this project you can do this with:
```
mkvirtualenv linguaggi_dinamici
workon linguaggi_dinamici
``` 

After that you can install the dependecies with ```pip```
```
pip install -r requirements.txt
```

## Setup the database

This project has been implemented using ```sqlite3``` for simplicity reasons, so if you wish to change DBMS know that it
has not been tested with any other.

Now that the dependecies have been installed you are ready to setup the datasource with ```manage.py```.
```
python manage.py makemigrations ecommerce
python manage.py migrate
```

## Fill the datasource with example data

This project has extended the manage.py functionalities in order to generate some DEMO data.
If you wish to generate some data you can do it with:
```
python manage.py populate_db
```