from django.contrib import admin
from .models import (Manufacturer, Category, Product,
                     Review, Order, OrderProduct, Model)


class ReviewAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['supplier']}),
        ('Author information', {'fields': ['author', 'score', 'text', 'gallery']}),
    ]


class ProductAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Header', {'fields': ['name', 'supplier', 'score']}),
        ('Generic Information', {'fields': ['manufacturer', 'model', 'category']}),
        ('Money - related', {'fields': ['price', 'sale']}),
        ('Extra Information', {'fields': ['description']})
    ]


admin.site.register(Manufacturer)
admin.site.register(Model)
admin.site.register(Category)
admin.site.register(Product, ProductAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Order)
admin.site.register(OrderProduct)
