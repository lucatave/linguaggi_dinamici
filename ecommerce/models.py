from django.db import models
from django.db.models import Avg
from django.contrib.auth.models import User

from django.core.validators import MinValueValidator, MaxValueValidator

from .choices import CURRENCY_CHOICES, EURO


class Manufacturer(models.Model):
    name = models.CharField(max_length=50, primary_key=True)

    def __str__(self):
        return self.name


class Model(models.Model):
    name = models.CharField(max_length=50)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE,
                                     related_name="models")

    class Meta:
        unique_together = ('name', 'manufacturer')
        index_together = ('name', 'manufacturer')

    def __str__(self):
        return self.manufacturer.__str__() + ' ' + self.name


class Category(models.Model):
    name = models.CharField(max_length=50, primary_key=True)
    parent_category = models.ForeignKey("self", on_delete=models.CASCADE,
                                        null=True, blank=True,
                                        related_name="subcategory")

    @staticmethod
    def get_hot_category(n: int):
        return Category.objects.raw('''
        SELECT ec.name, count(*) as n
        FROM ecommerce_category as ec, ecommerce_product_category as epc,
        ecommerce_orderproduct as eop
        WHERE
        ec.name = epc.category_id and
        epc.product_id = eop.product_id
        GROUP BY ec.name
        ORDER BY n DESC
        LIMIT %s''', [n])

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('name', 'parent_category')
        index_together = ('name', 'parent_category')


class Gallery(models.Model):
    image = models.ImageField()


class Product(models.Model):
    name = models.CharField(max_length=50)
    supplier = models.ForeignKey(User, on_delete=models.CASCADE,
                                 related_name='products')
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE,
                                     related_name="products", null=True)
    model = models.ForeignKey(Model, on_delete=models.CASCADE,
                              related_name="products", null=True)
    price = models.FloatField(validators=[MinValueValidator(0)])
    score = models.FloatField(validators=[
        MinValueValidator(0), MaxValueValidator(5)])

    gallery = models.ManyToManyField(Gallery, related_name="products")
    category = models.ManyToManyField(Category, related_name='products')

    description = models.TextField(null=True)
    sale = models.FloatField(null=True)
    cover = models.ImageField(null=True)

    currency = models.CharField(max_length=3, choices=CURRENCY_CHOICES,
                                default=EURO)

    def get_recommendation(self, n):
        return Product.objects.raw('''
        SELECT p.id, count(*) as n
        FROM ecommerce_product as p, ecommerce_orderproduct op
        WHERE p.id = op.product_id AND p.id <> %s AND
        op.order_id IN (SELECT op.order_id
                        FROM ecommerce_orderproduct as op
                        WHERE op.product_id = %s)
        GROUP BY p.id
        ORDER BY n DESC
        LIMIT %s''', [self.id, self.id, n])

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.supplier.groups.filter(name="Supplier"):
            super(Product, self).save()
        else:
            raise Exception("Supplier user must be inside Supplier user group.")

    def get_score(self):
        query = Review.objects.filter(product__id=self.id).aggregate(Avg('score'))
        score = 0
        if query:
            result = query['score__avg']
            if result:
                score = int(result)
        return range(0, score)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('supplier', 'manufacturer', 'model')
        index_together = ('supplier', 'manufacturer', 'model')


class Review(models.Model):
    supplier = models.ForeignKey(User, on_delete=models.CASCADE,
                                 related_name="received_reviews")
    author = models.ForeignKey(User, on_delete=models.CASCADE,
                               related_name="written_reviews")
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                related_name="reviews", null=True)

    gallery = models.ManyToManyField(Gallery, related_name="review")

    text = models.TextField()
    score = models.FloatField(validators=[
        MinValueValidator(0), MaxValueValidator(5)
    ])

    def __str__(self):
        return "Author " + self.author.username + "(" + str(self.score) + ") on " + self.product.name + " of " + \
               self.supplier.username

    def get_score(self):
        return range(0, self.score.as_integer_ratio()[0])


class Order(models.Model):
    buyer = models.ForeignKey(User, on_delete=models.CASCADE,
                              related_name="Orders")

    date = models.DateTimeField(auto_now=True)

    products = models.ManyToManyField(Product, through='OrderProduct',
                                      through_fields=('order', 'product'))
    open = models.BooleanField(null=False)

    def __str__(self):
        return str(self.id)


class OrderProduct(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                related_name="orders")
    quantity = models.IntegerField()

    def __str__(self):
        return "Order(" + str(self.order_id) + "): " + self.product.name + str(self.quantity)

    class Meta:
        unique_together = ('order', 'product')
        index_together = ('order', 'product')
