from typing import Tuple
from django.contrib.auth.models import User
from django import template

from ecommerce.models import Order
from ecommerce.utils import user_in_group, has_user_open_cart, get_user_open_cart

register = template.Library()


@register.filter
def in_group(user: User, group_name: str) -> bool:
    return user_in_group(user, group_name)


@register.simple_tag
def has_open_cart(user: User) -> bool:
    return has_user_open_cart(user.username)


@register.simple_tag
def get_cart(user: User) -> Order:
    return get_user_open_cart(user.username)
