from django.utils import timezone
from django.test import TestCase, Client
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from .models import (Manufacturer, Model, Product, Category,
                     Order, OrderProduct)


class TestHotCategories(TestCase):
    @classmethod
    def setUpTestData(cls):
        customer = Group.objects.create(name='Customer')
        supplier = Group.objects.create(name='Supplier')

        u1 = User.objects.create_user('alex', email='alex@example.com',
                                      password='alexpassword')
        u1.groups.add(customer)

        u2 = User.objects.create_user('lucatave',
                                      email='lucatave@example.com',
                                      password='lucatavepassword')
        u2.groups.add(customer)

        u3 = User.objects.create_user('giorgio', email='giorgio@example.com',
                                      password='giorgiopassword')
        u3.groups.add(customer)

        u4 = User.objects.create_user('simona', email='simona@example.com',
                                      password='simonapassword')
        u4.groups.add(customer)

        catE = Category.objects.create(name='Electronics')

        catSSD = Category.objects.create(name='SSD',
                                         parent_category=catE)

        catLED = Category.objects.create(name='LED',
                                         parent_category=catE)

        catRAM = Category.objects.create(name='RAM',
                                         parent_category=catE)

        sup1 = User.objects.create_user('samsung_supplier',
                                        email='samsung_supplier@example.com',
                                        password='samsungsupplierpassword')
        sup1.groups.add(supplier)

        man1 = Manufacturer.objects.create(name='Samsung')
        mod11 = Model.objects.create(name='EVO 950', manufacturer=man1)

        p1 = Product.objects.create(name='Samsung EVO 950 500GB',
                                    supplier=sup1,
                                    manufacturer=man1,
                                    model=mod11,
                                    price=130.90,
                                    score=3.0)

        p1.category.add(catSSD)
        o1 = Order.objects.create(buyer=u1, date=timezone.now(), open=False)

        op1 = OrderProduct.objects.create(order=o1, product=p1, quantity=2)


        sup2 = User.objects.create_user('led_supplier',
                                        email='led_supplier@example.com',
                                        password='ledsupplierpassword')
        sup2.groups.add(supplier)

        man2 = Manufacturer.objects.create(name='Minify')
        mod21 = Model.objects.create(name='V1', manufacturer=man2)

        p2 = Product.objects.create(name='Minify led supercool V2',
                                    supplier=sup2,
                                    manufacturer=man2,
                                    model=mod21,
                                    price=25.0,
                                    score=4.9)

        p2.category.add(catLED)
        o2 = Order.objects.create(buyer=u2, date=timezone.now(), open=False)

        op2 = OrderProduct.objects.create(order=o1, product=p2, quantity=1)

        sup3 = User.objects.create_user('ram_supplier',
                                        email='ram_supplier@example.com',
                                        password='ramsupplierpassword')
        sup3.groups.add(supplier)

        man3 = Manufacturer.objects.create(name='Corsair')
        mod31 = Model.objects.create(name='RAM DDR4 8GB',
                                     manufacturer=man3)

        p3 = Product.objects.create(name='RAM DDR4 8GB Corsair',
                                    supplier=sup3,
                                    manufacturer=man3,
                                    model=mod31,
                                    price=40,
                                    score=3.0)

        p3.category.add(catRAM)

        o3 = Order.objects.create(buyer=u3, date=timezone.now(), open=False)

        op3 = OrderProduct.objects.create(order=o3, product=p3, quantity=2)

        o4 = Order.objects.create(buyer=u4, date=timezone.now(), open=False)

        op4 = OrderProduct.objects.create(order=o4, product=p2, quantity=1)

        o5 = Order.objects.create(buyer=u1, date=timezone.now(), open=False)

        op5 = OrderProduct.objects.create(order=o5, product=p3, quantity=2)

        o6 = Order.objects.create(buyer=u2, date=timezone.now(), open=False)

        op6 = OrderProduct.objects.create(order=o6, product=p3, quantity=2)

    def test_hot_category(self):
        catE = Category(name='Electronics')
        catLED = Category(name='LED', parent_category=catE)
        catRAM = Category(name='RAM', parent_category=catE)

        self.assertEquals(Category.get_hot_category(2)[:2],
                          [catRAM, catLED])

    def test_index_response_contains_RAM_hot_category(self):
        client = Client()
        response = client.get('/ecommerce/')
        self.assertContains(response, 'RAM')

    def test_index_response_contains_LED_hot_category(self):
        client = Client()
        response = client.get('/ecommerce/')
        self.assertContains(response, 'LED')


class TestPermission(TestCase):
    @classmethod
    def setUpTestData(cls):
        customer = Group.objects.create(name='Customer')
        supplier = Group.objects.create(name='Supplier')

        user = User.objects.create_user('luca',
                                        email='luca@example.com',
                                        password='lucapassword')

        user.groups.add(customer)

        catE = Category.objects.create(name='Electronics')
        catSSD = Category.objects.create(name='SSD',
                                         parent_category=catE)

        catLED = Category.objects.create(name='LED',
                                         parent_category=catE)

        catRAM = Category.objects.create(name='RAM',
                                         parent_category=catE)

        catMouse = Category.objects.create(name='Mouse',
                                           parent_category=catE)

        sup1 = User.objects.create_user('samsung_supplier',
                                        email='samsung_supplier@example.com',
                                        password='samsungsupplierpassword')
        sup1.groups.add(supplier)

        man1 = Manufacturer.objects.create(name='Samsung')
        mod11 = Model.objects.create(name='EVO 950', manufacturer=man1)

        p1 = Product.objects.create(name='Samsung EVO 950 500GB',
                                    supplier=sup1,
                                    manufacturer=man1,
                                    model=mod11,
                                    price=130.90,
                                    score=3.0)

        p1.category.add(catSSD)

        sup2 = User.objects.create_user('led_supplier',
                                        email='led_supplier@example.com',
                                        password='ledsupplierpassword')
        sup2.groups.add(supplier)

        man2 = Manufacturer.objects.create(name='Minify')
        mod21 = Model.objects.create(name='V1', manufacturer=man2)

        p2 = Product.objects.create(name='Minify led supercool V2',
                                    supplier=sup2,
                                    manufacturer=man2,
                                    model=mod21,
                                    price=25.0,
                                    score=4.9)

        p2.category.add(catLED)

        sup3 = User.objects.create_user('ram_supplier',
                                        email='ram_supplier@example.com',
                                        password='ramsupplierpassword')
        sup3.groups.add(supplier)

        man3 = Manufacturer.objects.create(name='Corsair')
        mod31 = Model.objects.create(name='RAM DDR4 8GB',
                                     manufacturer=man3)

        p3 = Product.objects.create(name='RAM DDR4 8GB Corsair',
                                    supplier=sup3,
                                    manufacturer=man3,
                                    model=mod31,
                                    price=40,
                                    score=3.0)

        p3.category.add(catRAM)

        sup4 = User.objects.create_user('mouse_supplier',
                                        email='mouse_supplier@example.com',
                                        password='mousepassword')
        sup4.groups.add(supplier)

        man4 = Manufacturer.objects.create(name='Steelseries')
        mod41 = Model.objects.create(name='EC1',
                                     manufacturer=man4)

        p4 = Product.objects.create(name='Steelseries EC1 Black',
                                    supplier=sup4,
                                    manufacturer=man4,
                                    model=mod41,
                                    price=79.99,
                                    score=4.0)

        o1 = Order.objects.create(buyer=user, date=timezone.now(), open=False)

        op1 = OrderProduct.objects.create(order=o1, product=p1, quantity=2)
        op2 = OrderProduct.objects.create(order=o1, product=p2, quantity=1)

        o2 = Order.objects.create(buyer=user, date=timezone.now(), open=False)

        op3 = OrderProduct.objects.create(order=o2, product=p3, quantity=1)
        op4 = OrderProduct.objects.create(order=o2, product=p4, quantity=2)

    def test_login_direct(self):
        client = Client()
        response = client.post('/ecommerce/login/', {
            'username': 'luca',
            'password': 'lucapassword',
            'next': '/ecommerce/'})
        self.assertRedirects(response, '/ecommerce/')

    def test_signup_correct(self):
        client = Client()

        form_post = {'username': 'test_user',
                     'email': 'test_user_email@email.com',
                     'password1': 'test_user_password1',
                     'password2': 'test_user_password1',
                     'group': '1'}
        response = client.post('/ecommerce/signup/', form_post)
        self.assertRedirects(response, '/ecommerce/')
        user = User.objects.get(username='test_user')
        self.assertIsNotNone(user)
        self.assertEqual(user.email.__str__(), "test_user_email@email.com")

    def test_user_order_authenticated(self):
        client = Client()
        response = client.post('/ecommerce/login/', {
            'username': 'luca',
            'password': 'lucapassword',
            'next': '/ecommerce/'})
        response = client.get('/ecommerce/orders/')
        orders = response.context['orders']
        self.assertEqual(2, len(orders))

    def test_user_order_not_autheticated(self):
        client = Client()
        response = client.get('/ecommerce/orders/')
        self.assertRedirects(response,
                             '/ecommerce/login/')


class TestProduct(TestCase):
    @classmethod
    def setUpTestData(cls):
        customer = Group.objects.create(name='Customer')
        supplier = Group.objects.create(name='Supplier')

        user = User.objects.create_user('luca',
                                        email='luca@example.com',
                                        password='lucapassword')
        user.groups.add(customer)

        catE = Category.objects.create(name='Electronics')
        catSSD = Category.objects.create(name='SSD',
                                         parent_category=catE)

        catLED = Category.objects.create(name='LED',
                                         parent_category=catE)

        catRAM = Category.objects.create(name='RAM',
                                         parent_category=catE)

        sup1 = User.objects.create_user('samsung_supplier',
                                        email='samsung_supplier@example.com',
                                        password='samsungsupplierpassword')
        sup1.groups.add(supplier)

        man1 = Manufacturer.objects.create(name='Samsung')
        mod11 = Model.objects.create(name='EVO 950', manufacturer=man1)

        p1 = Product.objects.create(name='Samsung EVO 950 500GB',
                                    supplier=sup1,
                                    manufacturer=man1,
                                    model=mod11,
                                    price=130.90,
                                    score=3.0)

        p1.category.add(catSSD)

        sup2 = User.objects.create_user('led_supplier',
                                        email='led_supplier@example.com',
                                        password='ledsupplierpassword')
        sup2.groups.add(supplier)
        man2 = Manufacturer.objects.create(name='Minify')
        mod21 = Model.objects.create(name='V1', manufacturer=man2)

        p2 = Product.objects.create(name='Minify led supercool V2',
                                    supplier=sup2,
                                    manufacturer=man2,
                                    model=mod21,
                                    price=25.0,
                                    score=4.9)

        p2.category.add(catLED)

        sup3 = User.objects.create_user('ram_supplier',
                                        email='ram_supplier@example.com',
                                        password='ramsupplierpassword')
        sup3.groups.add(supplier)
        man3 = Manufacturer.objects.create(name='Corsair')
        mod31 = Model.objects.create(name='RAM DDR4 8GB',
                                     manufacturer=man3)

        p3 = Product.objects.create(name='RAM DDR4 8GB Corsair',
                                    supplier=sup3,
                                    manufacturer=man3,
                                    model=mod31,
                                    price=40,
                                    score=3.0)

        p3.category.add(catRAM)

        sup4 = User.objects.create_user('mouse_supplier',
                                        email='mouse_supplier@example.com',
                                        password='mousepassword')
        sup4.groups.add(supplier)
        man4 = Manufacturer.objects.create(name='Steelseries')
        mod41 = Model.objects.create(name='EC1',
                                     manufacturer=man4)

        p4 = Product.objects.create(name='Steelseries EC1 Black',
                                    supplier=sup4,
                                    manufacturer=man4,
                                    model=mod41,
                                    price=79.99,
                                    score=4.0)

        o1 = Order.objects.create(buyer=user, date=timezone.now(), open=False)

        op1 = OrderProduct.objects.create(order=o1, product=p1, quantity=2)
        op2 = OrderProduct.objects.create(order=o1, product=p2, quantity=1)
        op3 = OrderProduct.objects.create(order=o1, product=p3, quantity=1)

        o2 = Order.objects.create(buyer=user, date=timezone.now(), open=False)

        op3 = OrderProduct.objects.create(order=o2, product=p3, quantity=1)
        op4 = OrderProduct.objects.create(order=o2, product=p4, quantity=2)
        op5 = OrderProduct.objects.create(order=o2, product=p1, quantity=1)


    def test_product_recommendation(self):
        product = Product.objects.get(pk=1)
        works = False
        for r in product.get_recommendation(5):
            if r.name == "RAM DDR4 8GB Corsair":
                works = True

        self.assertEqual(True, works)