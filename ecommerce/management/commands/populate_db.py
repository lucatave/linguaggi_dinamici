from typing import Tuple
from django.core.files import File
from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group
from django.utils import timezone

from ecommerce.models import User, Category, Model, Manufacturer, Product, Order, OrderProduct
from ecommerce.choices import EURO


class Command(BaseCommand):
    help = 'No arguments needed. It just dumps some data into the db'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.supplier: Group = Group.objects.create(name='Supplier')
        self.customer: Group = Group.objects.create(name='Customer')

    def _create_user(self, username: str, customer_flag: bool = True) -> User:
        _default_password: str = 'blablabla'
        user = User.objects.create_user(username, username + '@email.com', _default_password)
        if customer_flag:
            user.groups.add(self.customer)
        else:
            user.groups.add(self.supplier)

        user.save()
        return user

    def create_customer(self, username: str) -> User:
        return self._create_user(username, True)

    def create_supplier(self, username: str) -> User:
        return self._create_user(username, False)

    def create_category(self, name: str) -> Category:
        return Category.objects.create(name=name)

    def create_model(self, name: str, m: Manufacturer) -> Model:
        return Model.objects.create(name=name, manufacturer=m)

    def create_manufacturer(self, name: str) -> Manufacturer:
        return Manufacturer.objects.create(name=name)

    def create_product(self, name, supplier: User, man: Manufacturer, mod: Model, price: float, c: Category):
        image = open('ecommerce/dummy_data/dummy_image.jpg', 'rb')
        image_wrap = File(image)

        product:Product = Product()
        product.supplier = supplier
        product.name = name
        product.model = mod
        product.manufacturer = man
        product.price = price
        product.score = 0.0

        product.save()

        product.category.add(c)

        product.cover.save('dummy_image-p'+ str(product.id) +'.jpg', image_wrap, save=True)
        product.save()

        return product

    def create_order(self, buyer: User, open: bool = False):
        return Order.objects.create(buyer=buyer, date=timezone.now(), open=open)

    def add_product_to_order(self, o: Order, p: Product, quantity: int):
        return OrderProduct.objects.create(order=o, product=p, quantity=quantity)

    def handle(self, *args, **options):
        u1: User = self.create_customer('customer_1')
        u2: User = self.create_customer('customer_2')
        u3: User = self.create_customer('customer_3')

        s1: User = self.create_supplier('supplier_1')
        s2: User = self.create_supplier('supplier_2')
        s3: User = self.create_supplier('supplier_3')

        c1: Category = self.create_category('dummy_category_1')
        c2: Category = self.create_category('dummy_category_2')

        man1: Manufacturer = self.create_manufacturer('manufacturer_1')
        man2: Manufacturer = self.create_manufacturer('manufacturer_2')

        mod11: Model = self.create_model('model_1', man1)
        mod12: Model = self.create_model('model_2', man1)

        mod21: Model = self.create_model('model__1', man2)
        mod22: Model = self.create_model('model__2', man2)

        p1: Product = self.create_product('amazing product 1', s1, man1, mod11, 15, c1)
        p2: Product = self.create_product('amazing product 2', s1, man1, mod12, 20, c2)

        p3: Product = self.create_product('wonderful product 1', s2, man1, mod11, 17, c1)
        p4: Product = self.create_product('wonderful product 2', s2, man2, mod22, 23, c2)

        p5: Product = self.create_product('wonderful product 2', s3, man2, mod21, 30, c2)
        p6: Product = self.create_product('wonderful product 2', s3, man2, mod22, 34, c2)

        o1: Order = self.create_order(u1)
        self.add_product_to_order(o1, p1, 1)
        self.add_product_to_order(o1, p2, 1)

        o2: Order = self.create_order(u2)
        self.add_product_to_order(o2, p3, 1)
        self.add_product_to_order(o2, p4, 1)

        o3: Order = self.create_order(u3)
        self.add_product_to_order(o3, p5, 1)
        self.add_product_to_order(o3, p6, 1)
