EURO = '€'
DOLLAR = '$'
DEFAULT_CURRENCY = EURO
CURRENCY_CHOICES = ((EURO, 'EUR'), (DOLLAR, 'USD'))

GROUP_CHOICES = (
    (1, "Supplier"),
    (2, "Customer"))
