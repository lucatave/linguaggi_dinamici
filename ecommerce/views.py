from django.shortcuts import render, get_list_or_404, get_object_or_404, redirect
from django.http import HttpResponseRedirect, Http404
from django.db import transaction
from django.db.utils import IntegrityError
from django.db.models import Q
from django.views import generic
from django.contrib.auth import login, authenticate
from ecommerce.forms import MyUserCreationForm, ReviewForm, ProductForm

from ecommerce.choices import CURRENCY_CHOICES
from .models import (Category, Product, Review, Order)
from .utils import (user_in_group, get_orders_by_user, get_products_by_supplier, paginator_helper,
                    create_product_from_http_request, get_choices, add_product_to_cart, close_order,
                    get_average_score_by_supplier, get_number_of_orders_on_product, update_product_from_http_request,
                    get_products_by_supplier_username)


class IndexView(generic.ListView):
    template_name = 'ecommerce/index.html'
    context_object_name = 'categories'

    def get_queryset(self):
        return Category.get_hot_category(10)


def orders_view(request):
    orders = []
    if request.user.is_authenticated:
        orders = paginator_helper(request, get_orders_by_user(request.user))
        context = {'orders': orders}
        return render(request, 'ecommerce/order_list.html', context)
    return redirect('ecommerce:login')


def order_detail(request, order_id: int):
    products = []
    context = {}
    if request.user.is_authenticated:
        order: Order = Order()
        try:
            order = Order.objects.get(id=order_id)
        except Order.DoesNotExist:
            redirect('ecommerce:index')

        if order.buyer_id != request.user.id:
            return redirect('ecommerce:index')

        if order:
            products = paginator_helper(request, order.products.all())
            context = {
                'products': products,
                'order': order
            }
            if request.method == 'POST':
                close_order(order_id)
                return redirect('ecommerce:index')

    return render(request, 'ecommerce/order_detail.html', context)


def cancel_order(request, order_id: int):
    if request.user.is_authenticated:

        order: Order = Order.objects.get(id=order_id)
        if order.buyer_id != request.user.id:
            return redirect('ecommerce:index')

        order.delete()

    return redirect('ecommerce:index')


def supplier_overview(request, username: str):
    context = {}
    if request.user.is_authenticated:
        context['supplier_username'] = username
        context['average_score'] = range(0, int(get_average_score_by_supplier(username)['score__avg']))
        context['products'] = paginator_helper(request, get_products_by_supplier_username(username))

        return render(request, 'ecommerce/supplier_overview.html', context)
    return redirect('ecommerce:index')


@transaction.atomic
def add_product_to_cart_request(request, product_id):
    if request.user.is_authenticated and request.method == 'POST':
        user = request.user
        if user_in_group(user, 'Customer'):
            add_product_to_cart(user, product_id)

    return redirect('ecommerce:product', product_id=product_id)


@transaction.atomic
def signup_view(request):
    if request.user.is_authenticated:
        redirect('ecommerce:index')

    if request.method == 'POST':
        form = MyUserCreationForm(request.POST)

        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)

            return redirect('ecommerce:index')

    else:
        form = MyUserCreationForm()

    context = {'form': form}
    return render(request, 'ecommerce/registration/sign_up.html', context)


def category(request, category_id):
    products = paginator_helper(request,
                                Product.objects.filter(category__name=category_id).order_by('-score').all(), 10)
    context = {'products': products,
               'category_id': category_id}
    return render(request, 'ecommerce/category_products.html', context)


def edit_product_view(request, product_id):
    if request.user.is_authenticated:
        user = request.user

        if user_in_group(user, 'Supplier'):
            product = get_object_or_404(Product, pk=product_id)
            if product.supplier_id == request.user.id:
                context = {}
                if request.method == 'GET':
                    form = ProductForm()

                    context = {'name': product.name,
                               'manufacturer': product.manufacturer.name,
                               'model': product.model.name,
                               'category': product.category.name,
                               'price': product.price,
                               'description': product.description}

                    return render(request, 'ecommerce/form_edit_product.html', context)
                if request.method == 'POST':
                    update_product_from_http_request(request, product_id)
                    return redirect('ecommerce:index')

    return redirect('ecommerce:index')


def product_view(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    try:
        reviews = get_list_or_404(Review, product=product_id)
    except Http404:
        reviews = []

    reviews = paginator_helper(request, reviews, 10)
    form = ReviewForm()
    gallery_images = product.gallery
    context = {'product': product,
               'recommendations': product.get_recommendation(3),
               'reviews': reviews,
               'gallery_images': gallery_images,
               'sell_number': get_number_of_orders_on_product(product_id),
               'form': form}

    return render(request, 'ecommerce/product.html', context)


@transaction.atomic
def submit_review(request, product_id):
    if request.method == 'POST':
        product = Product.objects.get(id=str(product_id))
        if request.user.username != product.supplier.username:
            form = ReviewForm(request.POST, request.FILES)

            if form.is_valid():
                author = request.user.username
                form.cleaned_data['author'] = author
                form.cleaned_data['supplier'] = product.supplier.username
                form.cleaned_data['author'] = author
                form.cleaned_data['product'] = product.id

                form.save()

    return redirect('ecommerce:product', product_id=product_id)


def search_view(request):
    products = []
    context = {}
    if request.method == 'GET':
        search_query = request.GET.get('searchbox', '')
        context['search_query'] = search_query
        products = paginator_helper(request, Product.objects.filter(
            Q(name__contains=search_query) |
            Q(model__manufacturer__name__contains=search_query) |
            Q(model__name__contains=search_query) |
            Q(supplier__username__contains=search_query)).order_by('score'))
    context['products'] = products
    return render(request, 'ecommerce/products.html', context)


def profile_view(request):
    user = request.user
    if user.is_authenticated:
        customer = "Customer"
        if user_in_group(user, customer):
            orders = paginator_helper(request, get_orders_by_user(user), 5)
            context = {'orders': orders}
            return render(request, 'ecommerce/customer_profile.html', context)
        else:
            message = ""
            context = {}
            try:
                handle_product_form(request, context)
            except IntegrityError:
                message = 'You are already selling this product.'

            products = paginator_helper(request, get_products_by_supplier(user), 5)
            average_score = get_average_score_by_supplier(user.username)['score__avg']

            if average_score is None:
                average_score = 0

            context['products'] = products
            context['currency_choices'] = get_choices(CURRENCY_CHOICES)
            context['average_score'] = range(0, int(average_score))

            return render(request, 'ecommerce/supplier_profile.html', context)

    return redirect('ecommerce:index')


def handle_product_form(request, context):
    if request.method == 'GET':
        context['form'] = ProductForm()
    elif request.method == 'POST':
        create_product_from_http_request(request)

