"""
Utils function should be put here
"""
from typing import List, Tuple

from django.contrib.auth.models import User, Group
from django.db.models import QuerySet, Avg
from django.db import transaction
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from ecommerce.choices import GROUP_CHOICES
from ecommerce.models import Product, Category, Manufacturer, Model, Order, OrderProduct
from ecommerce.forms import ProductForm, EditProductForm


def paginator_helper(request, list_to_paginate, element_per_page=25):
    paginator: Paginator = Paginator(list_to_paginate, element_per_page)
    page = request.GET.get('page')

    try:
        paged_list = paginator.page(page)
    except PageNotAnInteger:
        paged_list = paginator.page(1)
    except EmptyPage:
        paged_list = paginator.page(paginator.num_pages)

    return paged_list


def create_category_if_needed(category_name: str) -> Category:
    category: Category = Category.objects.filter(name=category_name)
    if not category:
        category = Category()
        category.name = category_name
        category.save()

    return category


def create_manufacturer(manufacturer_name: str) -> Manufacturer:
    manufacturer: Manufacturer = Manufacturer()
    manufacturer.name = manufacturer_name

    manufacturer.save()
    return manufacturer


def create_model(model_name: str, manufacturer: Manufacturer) -> Model:
    model: Model = Model()
    model.manufacturer = manufacturer
    model.name = model_name

    model.save()
    return model


def create_manufacturer_model_if_needed(manufacturer_name: str, model_name: str) -> bool:
    manufacturer: Manufacturer = Manufacturer.objects.filter(name=manufacturer_name)
    if manufacturer:
        manufacturer = Manufacturer.objects.get(name=manufacturer_name)
        if Model.objects.filter(manufacturer=manufacturer, name=model_name):
            return True
        else:
            create_model(model_name, manufacturer)
    else:
        manufacturer = create_manufacturer(manufacturer_name)
        create_model(model_name, manufacturer)

    return False


def create_product_entry_in_order(order: Order, product_id: str) -> OrderProduct:
    product_entry: OrderProduct = OrderProduct()

    product_entry.order = order
    product_entry.product = Product.objects.get(id=product_id)
    product_entry.quantity = 1

    product_entry.save()
    return product_entry


@transaction.atomic
def create_product_from_http_request(request) -> bool:
    form = ProductForm(request.POST, request.FILES)
    print(form.errors)
    if form.is_valid():
        form.cleaned_data['supplier'] = request.user.username

        create_category_if_needed(form.cleaned_data['category'])

        create_manufacturer_model_if_needed(form.cleaned_data['manufacturer'],
                                            form.cleaned_data['model'])
        form.save()
        return True

    return False


@transaction.atomic
def update_product_from_http_request(request, product_id: int) -> Product:
    product: Product = Product.objects.get(id=product_id)

    form = EditProductForm(request.POST)
    if form.is_valid():
        product.price = form.cleaned_data['price']
        product.description = form.cleaned_data['description']

        product.save()

    return product


def user_in_group(user: User, group_name: str) -> bool:
    group = Group.objects.get(name=group_name)
    return group in user.groups.all()


def get_available_group_names() -> List[str]:
    l: List[str] = []
    for key, val in GROUP_CHOICES:
        l.append(val)

    return l


def get_orders_by_user(user: User) -> QuerySet:
    return Order.objects.filter(buyer=user).order_by('id')


def get_products_by_supplier(user: User) -> QuerySet:
    return Product.objects.filter(supplier=user).order_by('-score')


def get_products_by_supplier_username(username: str) -> QuerySet:
    try:
        user = User.objects.get(username=username)
        return get_products_by_supplier(user)
    except User.DoesNotExist:
        return QuerySet()


def get_choices(choices: Tuple[Tuple[int, str]]) -> List[str]:
    choices_list: List[str] = []
    for k, v in choices:
        choices_list.append(v)

    return choices_list


def has_user_open_cart(username: str) -> bool:
    return Order.objects.filter(buyer__username=username, open=True).count() > 0


def get_user_open_cart(username: str) -> Order:
    return Order.objects.get(buyer__username=username, open=True)


def exists_product_in_cart(product_id: str, order_id: int) -> bool:
    return OrderProduct.objects.filter(order_id=order_id, product_id=product_id).count() > 0


def get_product_in_cart(product_id: str, order_id: int) -> OrderProduct:
    return OrderProduct.objects.get(order_id=order_id, product_id=product_id)


@transaction.atomic
def add_product_to_cart(user: User, product_id: str) -> Order:
    order: Order = None
    if has_user_open_cart(user.username):
        order = get_user_open_cart(user.username)
    else:
        order = Order()
        order.buyer = user
        order.open = True

        order.save()

    if exists_product_in_cart(product_id, order.id):
        product_entry: OrderProduct = get_product_in_cart(product_id, order.id)
        product_entry.quantity += 1

        product_entry.save()

    else:
        create_product_entry_in_order(order, product_id)


@transaction.atomic
def close_order(order_id: int):
    order: Order = Order.objects.get(id=order_id)
    order.open = False

    order.save()


def get_average_score_by_supplier(supplier_username: str) -> float:
    average = Product.objects.filter(supplier__username=supplier_username).aggregate(Avg('score'))
    return average


def get_number_of_orders_on_product(product_id: int) -> int:
    return OrderProduct.objects.filter(product_id=product_id).count()