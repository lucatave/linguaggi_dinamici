from django.contrib.auth.forms import UserCreationForm, UsernameField
from django.contrib.auth.models import User, Group
from django import forms
from django.db.models import Avg
from . import models
from .choices import GROUP_CHOICES, DEFAULT_CURRENCY


class MyUserCreationForm(UserCreationForm):
    group = forms.ChoiceField(choices=GROUP_CHOICES, widget=forms.RadioSelect)

    class Meta:
        model = User
        fields = ("username", "email",)
        field_classes = {'username': UsernameField,
                         "email": forms.EmailField}

    def save(self, commit=True):
        user = super(MyUserCreationForm, self).save(commit=False)

        if commit:
            user.save()

            selected_group_name = self.cleaned_data.get('group')
            selected_group = Group.objects.get(pk=selected_group_name)

            selected_group.user_set.add()

            user.save()
            user.groups.add(selected_group)
            user.save()


class ReviewForm(forms.Form):
    review_image = forms.ImageField(required=False)
    supplier = forms.HiddenInput()
    author = forms.HiddenInput()
    text = forms.CharField(widget=forms.Textarea)
    score = forms.IntegerField(min_value=1, max_value=5, required=True)

    def save(self, commit=True):
        if commit:
            review = models.Review()
            product = self.cleaned_data['product']

            product_model = models.Product.objects.get(pk=product)
            review.product = product_model

            supplier = self.cleaned_data['supplier']
            supplier_model = User.objects.get(username=supplier)
            review.supplier = supplier_model

            author = self.cleaned_data['author']
            author_model = User.objects.get(username=author)
            review.author = author_model

            text_field = self.cleaned_data['text']
            review.text = text_field

            score = self.cleaned_data['score']
            review.score = score

            review.save()

            gallery = models.Gallery()
            gallery.image = self.cleaned_data['review_image']
            if gallery.image:
                gallery.save()
                review.gallery.add(gallery)
                review.save()

            product_model.score = product_model.reviews.aggregate(Avg('score'))['score__avg']
            product_model.save()


class EditProductForm(forms.Form):
    price = forms.CharField(required=True)
    description = forms.CharField(required=True)


class ProductForm(forms.Form):
    name = forms.CharField(required=True)
    manufacturer = forms.CharField(required=True)
    model = forms.CharField(required=True)
    category = forms.CharField(required=True)
    price = forms.CharField(required=True)
    description = forms.CharField(required=True)
    supplier = forms.HiddenInput()
    cover = forms.ImageField(required=True)

    def save(self, commit=True):
        if commit:
            product = models.Product()

            product.name = self.cleaned_data['name']
            product.supplier = User.objects.get(username=self.cleaned_data['supplier'])
            product.manufacturer = models.Manufacturer.objects.get(name=self.cleaned_data['manufacturer'])
            product.model = models.Model.objects.get(name=self.cleaned_data['model'])
            product.price = self.cleaned_data['price']
            product.cover = self.cleaned_data['cover']
            product.currency = DEFAULT_CURRENCY
            product.score = 0;

            product.save()
            product.category.add(models.Category.objects.get(name=self.cleaned_data['category']))
            product.save()
