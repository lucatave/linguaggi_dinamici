from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.views.static import serve
from . import views

app_name = 'ecommerce'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^login/$', auth_views.LoginView.as_view(
        template_name='ecommerce/registration/login.html'), name='login'),
    url(r'^signup/$', views.signup_view, name='signup'),
    url(r'^logout/$', auth_views.LogoutView.as_view(
        next_page=settings.DEFAULT_REDIRECT_URL), name='logout'),
    url(r'^category/(?P<category_id>\w+)/$', views.category,  name='category'),
    url(r'^orders/$', views.orders_view, name='orders'),
    url(r'^order/(?P<order_id>[0-9]+)/$', views.order_detail,
        name='order'),
    url(r'^order/confirm/(?P<order_id>[0-9]+)/$', views.order_detail,
        name='close_order'),
    url(r'^order/cancel/(?P<order_id>[0-9]+)/$', views.cancel_order,
        name='cancel_order'),
    url(r'^add_to_cart/(?P<product_id>[0-9]+)/$', views.add_product_to_cart_request,
        name='add_to_cart'),
    url(r'^product/(?P<product_id>[0-9]+)/$', views.product_view,
        name='product'),
    url(r'^product/edit/(?P<product_id>[0-9]+)/$', views.edit_product_view,
        name='edit_product'),
    url(r'^supplier/(?P<username>\w+)/$', views.supplier_overview,
        name='supplier_overview'),
    url(r'^product/(?P<product_id>[0-9]+)/review/$', views.submit_review, name='submit_review'),
    url(r'^search/$', views.search_view, name='search_view'),
    url(r'^profile/$', views.profile_view, name='profile')
]
